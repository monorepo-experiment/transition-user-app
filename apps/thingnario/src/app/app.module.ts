import { FeatureEmsModule } from '@transition-user-app/feature/ems';
import { FeatureVppModule } from '@transition-user-app/feature/vpp';
import { Module } from '@nestjs/common';

import { AppController } from './app.controller';
import { AppService } from './app.service';

@Module({
  imports: [FeatureEmsModule, FeatureVppModule],
  controllers: [AppController],
  providers: [AppService],
})
export class AppModule {}
