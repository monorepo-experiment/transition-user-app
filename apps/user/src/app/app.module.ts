import { Module } from '@nestjs/common';
import { FeatureUserModule } from '@transition-user-app/feature/user';

import { AppController } from './app.controller';
import { AppService } from './app.service';

@Module({
  imports: [FeatureUserModule],
  controllers: [AppController],
  providers: [AppService],
})
export class AppModule {}
