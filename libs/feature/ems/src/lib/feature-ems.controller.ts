import { SharedUserService } from '@transition-user-app/shared/user';
import { Controller, Get, Query } from '@nestjs/common';

@Controller('ems')
export class FeatureEmsController {
  constructor(private userService: SharedUserService) {}

  @Get('hello')
  async hello(@Query('token') token: string): Promise<string> {
    if (!(await this.userService.validate(token))) {
      return 'invalid';
    }
    return 'world ems';
  }
}
