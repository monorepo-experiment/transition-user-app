import { Module } from '@nestjs/common';
import { FeatureUserController } from './feature-user.controller';
import { FeatureUserService } from './feature-user.service';

@Module({
  controllers: [FeatureUserController],
  providers: [FeatureUserService],
  exports: [],
})
export class FeatureUserModule {}
