import { Controller, Get, Query } from '@nestjs/common';
import { FeatureUserService } from './feature-user.service';

@Controller('user')
export class FeatureUserController {
  constructor(private userService: FeatureUserService) {}

  @Get('validate')
  hello(@Query('token') token: string): boolean {
    return this.userService.validate(token);
  }
}
