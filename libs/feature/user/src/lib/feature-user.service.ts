import { Injectable } from '@nestjs/common';

@Injectable()
export class FeatureUserService {
  validate(token: string) {
    return token === 'thingnario';
  }
}
