import { HttpService } from '@nestjs/axios';
import { Injectable } from '@nestjs/common';
import { firstValueFrom } from 'rxjs';

@Injectable()
export class SharedUserService {
  constructor(private readonly httpService: HttpService) {}

  async validate(token: string): Promise<boolean> {
    const response = await firstValueFrom(
      this.httpService.get<boolean>(
        `http://localhost:4444/api/user/validate?token=${token}`
      )
    );

    return response.data;
  }
}
