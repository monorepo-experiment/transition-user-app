import { HttpModule } from '@nestjs/axios';
import { Module } from '@nestjs/common';
import { SharedUserService } from './shared-user.service';

@Module({
  imports: [HttpModule],
  providers: [SharedUserService],
  exports: [SharedUserService],
})
export class SharedUserModule {}
